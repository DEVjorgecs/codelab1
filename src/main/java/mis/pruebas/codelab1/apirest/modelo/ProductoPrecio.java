package mis.pruebas.codelab1.apirest.modelo;

public class ProductoPrecio {

    private long id;
    private double precio;

    //Constructor
    public ProductoPrecio() {
    }


    public ProductoPrecio(double precio) {
        this.precio = precio;
    }



    //GETTERS y SETTERS
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
