package mis.pruebas.codelab1.apirest.modelo;

import java.util.List;
import java.util.Objects;

// POJO - Plain Old Java Objects
public class Producto {
    private long id;
    private String descripcion;
    private double precio;
    private List<Usuario> users;



    public Producto() {
    }

    //CONSTRUCTORES
    public Producto(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }


    public Producto(long id, String descripcion, double precio, List<Usuario> users) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.users = users;
    }

    //GETTERS
    public long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public List<Usuario> getUsers(){
        return this.users;
    }


    //SETTERS
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setUsers(List<Usuario> users) {
        this.users = users;
    }


    /* _____________________________________________

    // Metodo ToString
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductoModel{");
        sb.append("id=").append(id);
        sb.append(", descripcion='").append(descripcion).append('\'');
        sb.append(", precio=").append(precio);
        sb.append('}');
        return sb.toString();
    }

    // Metodo Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Producto)) return false;
        Producto producto = (Producto) o;
        return getId() == producto.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }


    */



}

