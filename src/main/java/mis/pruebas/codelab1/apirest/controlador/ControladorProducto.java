package mis.pruebas.codelab1.apirest.controlador;


import mis.pruebas.codelab1.apirest.modelo.Producto;
import mis.pruebas.codelab1.apirest.modelo.ProductoPrecio;
import mis.pruebas.codelab1.apirest.servicio.ServicioProducto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ControladorProducto {

    @Autowired
    private ServicioProducto servicioProducto;
/*
    @GetMapping("")
    public String root() {
        return "TechU API REST CodeLab1";
    }

    // GET - Cuenta el numero de productos
    @GetMapping("/productos/size")
    public int contarProductos() {
        return servicioProducto.obtenerProductos().size();
    }
*/

    // 1) GET - Obtiene todos los productos
    @GetMapping("/productos")
    public List<Producto> obtenerProductos() {
        return servicioProducto.obtenerProductos();
    }



    // 1) GET - Obtiene producto por Id
    @GetMapping("/productos/{id}")
    public ResponseEntity obtenerProductoPorId(@PathVariable int id) {
        Producto pr = servicioProducto.getProducto(id); //obtiene producto segun su id
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }


    // 2) POST - Crea/Agrega nuevos productos
    @PostMapping("/productos")
    public ResponseEntity<String> agregarProducto(@RequestBody Producto producto) {
        servicioProducto.agregarProducto(producto);
        return new ResponseEntity<>("Producto fue creado de manera exitosa!", HttpStatus.CREATED);
    }


    // 3) PUT - Update/Actualiza un producto
    @PutMapping("/productos/{id}")
    public ResponseEntity actualizarProducto(@PathVariable int id,
                                             @RequestBody Producto productoActualizar) {
        Producto pr = servicioProducto.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioProducto.actualizarProducto(id-1, productoActualizar);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }


    // 4) DELETE - Elimina un producto
    @DeleteMapping("/productos/{id}")
    public ResponseEntity eliminarProducto(@PathVariable Integer id) {
        Producto pr = servicioProducto.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioProducto.eliminarProducto(id - 1);
        return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.OK); // También puede mandar ERROR 204 No Content
    }


    // 5) PATCH - Actualiza precio de un producto
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoPrecio(
            @RequestBody ProductoPrecio productoPrecio, @PathVariable int id){
        Producto pr = servicioProducto.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecio.getPrecio()); //Actualiza el precio del producto
        servicioProducto.actualizarProducto(id-1, pr);

        return new ResponseEntity<>(pr, HttpStatus.OK); //Update OK
    }
    

    // 6) GET subrecurso - Obtiene el subrecurso de un producto
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        Producto pr = servicioProducto.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
