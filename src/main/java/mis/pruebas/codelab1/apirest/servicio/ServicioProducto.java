package mis.pruebas.codelab1.apirest.servicio;



import mis.pruebas.codelab1.apirest.modelo.Producto;
import mis.pruebas.codelab1.apirest.modelo.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class ServicioProducto {
    private List<Producto> listaProductos = new ArrayList<Producto>();

    public ServicioProducto() {
        listaProductos.add(new Producto(1, "producto 1", 400.50));
        listaProductos.add(new Producto(2, "producto 2", 50.00));
        listaProductos.add(new Producto(3, "producto 3", 100.00));
        listaProductos.add(new Producto(4, "producto 4", 99.99));
        listaProductos.add(new Producto(5, "producto 5", 120.00));
        List<Usuario> users = new ArrayList<>();
        users.add(new Usuario("1"));
        users.add(new Usuario("3"));
        users.add(new Usuario("5"));
        listaProductos.get(1).setUsers(users);
    }

    // Lee los productos y usuarios creados
    public List<Producto> obtenerProductos() {
        return listaProductos;
    }

    // READ instance
    public Producto getProducto(long index) throws IndexOutOfBoundsException {
        if(obtenerIndex(index)>=0) {
            return listaProductos.get(obtenerIndex(index));
        }
        return null;
    }

    // Crear/Agregar Producto
    public Producto agregarProducto(Producto prod) {
        listaProductos.add(prod);
        return prod;
    }


    // Actualizar Producto
    public Producto actualizarProducto(int index, Producto prod)
            throws IndexOutOfBoundsException {
        listaProductos.set(index, prod);
        return listaProductos.get(index);
    }

    // Eliminar productos
    public void eliminarProducto(int index) throws IndexOutOfBoundsException {
        int pos = listaProductos.indexOf(listaProductos.get(index));
        listaProductos.remove(pos);
    }


    // Obtener la posición/indice del producto
    public int obtenerIndex(long index) throws IndexOutOfBoundsException {
        int i=0;
        while(i<listaProductos.size()) {
            if(listaProductos.get(i).getId() == index){
                return(i); }
            i++;
        }
        return -1;
    }


}


